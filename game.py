import random
import os
import pickle
import numpy as np
import time

class Game():
    def __init__(self, target_loc: int, self_loc: int):
        self.board = []
        self.set_up_board()
        self.game_over = False
        self.self_loc = self_loc
        self.target_loc = target_loc
        self.draw_board(target_loc, self_loc)

    def set_up_board(self):
        self.board = [0, 0, 0, 0, 0, 0, 0]

    def draw_board(self, target_loc_index: int, self_loc_index: int):
        self.self_loc = self_loc_index
        self.target_loc = target_loc_index
        self.board[self.target_loc] = 1
        self.board[self.self_loc] = 2
        for (i, n) in enumerate(self.board):
            if i != self.target_loc and i != self.self_loc:
                self.board[i] = 0

    def rand_board(self):
        target = random.randrange(0, 7)
        while True:
            selfl = random.randrange(0, 7)
            if selfl != target:
                break
        self.draw_board(target, selfl)
        self.game_over = False
        return self.board

    def rand_move(self):
        lor = random.randrange(0, 2)
        reward, state = self.move(lor)
        action = lor
        return reward, state, action

    def move(self, left_or_right: int):
        reward = 0
        og_distance = abs(self.target_loc - self.self_loc)
        if left_or_right == 0:
            if self.self_loc == 0:
                # self.draw_board(self.target_loc, self.self_loc)
               reward = -1
            else:
                self.self_loc -= 1
                self.draw_board(self.target_loc, self.self_loc)

        elif left_or_right == 1:
            if self.self_loc == 6:
                # self.draw_board(self.target_loc, self.self_loc)
                reward = -1
            else:
                self.self_loc += 1
                self.draw_board(self.target_loc, self.self_loc)

        if self.self_loc == self.target_loc:
            reward, _ = self.end_game()

        new_distance = abs(self.target_loc - self.self_loc)
        if reward == 0:
            reward = og_distance - new_distance

        return reward, self.board

    def end_game(self):
        self.game_over = True
        return (10, self.board)


def person_play():
    g = Game(5, 0)
    g.rand_board()
    total_reward = 0
    while True:
        (r, s) = g.move(1)
        total_reward += r
        print("Total: {}, Reward: {}, State: {}".format(total_reward, r, s))
        if g.game_over:
            print("Game End")
            break

def ai_train():
    """setup"""
    game = Game(5, 0)
    state = game.rand_board()
    # states_possible = [0, 1]
    actions_possible = [0, 1]
    q_table = {}
    EPISODES = range(0, 9999999999999)
    epsilon = 0.9
    epsilon_min = 0.01
    epsilon_decay = 0.005
    alpha = 0.618
    # CUDA = ON
    """end_setup"""
    for episode in EPISODES:
        done = False
        reward = 1
        state = game.rand_board()
        # do thing every time step
        while not game.game_over:

            # check to see wether or not to do a random action
            random_value = random.random()
            if random_value <= epsilon:
                reward, next_state, action_to_take = game.rand_move()
            else:
                if repr(state) in q_table:
                    actions_in_q = q_table[repr(state)]
                    action_to_take = actions_in_q.index(max(actions_in_q))
                else:
                    q_table[repr(state)] = [0, 0]
                    action_to_take = 0
                reward, next_state = game.move(action_to_take)

            # next_state, reward, done, info = env.step(action)
            if repr(state) not in q_table:
                q_table[repr(state)] = [0, 0]
                action_to_take = 0

            q_table[repr(state)][action_to_take] += alpha * ((reward + (max(q_table[repr(next_state)]))) - q_table[repr(state)][action_to_take])

            # print(state, reward, action_to_take, next_state)
            state = next_state

            if epsilon <= epsilon_min:
                epsilon = epsilon_min
            else:
                epsilon -= epsilon_decay


        if episode % 1000 == 0:
            try:
                os.remove("out-{}-episodes".format(episode - 1000))
            except Exception as e:
                pass

            print(f'Episode {episode}', end=': ')
            outfile_name = "out-{}-episodes".format(episode)
            outfile = open(outfile_name, "wb")
            pickle.dump(q_table, outfile)
            outfile.close()
            print('saved')

    print(f"Done, {EPISODES} episodes !")

def ai_play(ep: int):
    import time

    """setup"""
    game = Game(2, 3)
    state = game.board
    EPISODES_YOU_GOT_TO = ep
    in_file = open("out-{}-episodes".format(EPISODES_YOU_GOT_TO), "rb")
    q_table = pickle.load(in_file)
    print(q_table)
    """end_setup"""

    done = False
    reward_total = 0
    state = game.rand_board()
    time_step = 0
    reward = 0
    # do thing every time step
    while not game.game_over:
        # print(game.board)
        print('''Reward: {}\n
               Reward Total: {}
               State: {}\n
               Time Step: {}\n
               '''.format(reward, reward_total, game.board, time_step))

        # action = np.argmax(q_table[state])
        action = q_table[repr(state)].index(max(q_table[repr(state)]))
        reward, next_state = game.move(action)
        state = next_state
        time_step += 1
        reward_total += reward
        time.sleep(0.5)

    print('''Reward: {}\n
                       Reward Total: {}
                       State: {}\n
                       Time Step: {}\n
                       '''.format(reward, reward_total, game.board, time_step))

    in_file.close()


# NN
def sigmoid(n):
    return 1 / (1 + np.exp(-n))
def sigmoid_derivative(n):
    return n * (1 - n)


class NN:
    # from numpy import exp, array, random, dot

    def __init__(self, game):

        np.random.seed(1)
        # this generates a numpy 3 x 1 matrix, the - one is to make sure there is negative numbers. But that makes it so
        # there is only negatives. so you multiply it by 2 before hand to make sure there is both positive and negative
        # range is (-1, 1)
        self.weights = 2 * np.random.random((14, 1))
        self.game = game
        self.error = 1

    def train(self, episodes: int):
        for n in range(episodes):
            # process input
            state = self.game.rand_board()

            while not self.game.game_over:
                board = self.game.board
                oneindex = board.index(1)
                twoindex = board.index(2)
                state = [0] * 14
                state[oneindex] = 1
                state[twoindex + 7] = 2
                state = np.array([state])
                output = self.think(state)
                # calculate error (desired - predicted) with matrix subtraction
                action = np.round(output)
                # action = 1 - action
                # print(action)

                reward, new_board = self.game.move(action)
                # print(action)
                if reward < 0:
                    reward = 0
                if reward > 1:
                    reward = 1
                # reward = abs(reward)
                error = (reward - output)
                # error = abs(error)

                self.error = error
                # print(error)
                # dot multiply the error by the input and again by the gradient of the Sigmoid curve.
                # This means less confident weights are adjusted more.
                # This means inputs, which are zero, do not cause changes to the weights.
                adjustment_to_make = np.dot(state.T, error * sigmoid_derivative(output))

                # add the adjustment value to their individual weights
                self.weights += adjustment_to_make
                # print(self.game.board)
            # print(f"Error: {self.error}")

            if n % 10:
                print(n)

    # make network do thinking.
    # only one neuron is in this network
    def think(self, inputs):
        # dot multiply inputs by weights
        out = np.dot(inputs, self.weights)
        # normalize it between 1 and 0
        normalized_out = sigmoid(out)
        return normalized_out


def nn_train(n: int):
    game = Game(0, 6)
    nn = NN(game)
    print("nn train started")
    nn.train(n)


def nn_play():
    game = Game(0, 6)
    nn = NN(game)
    done = False
    reward_total = 0
    state = game.rand_board()
    time_step = 0
    reward = 0
    while not game.game_over:
        # print(game.board)
        print('''Reward: {}\n
               Reward Total: {}
               State: {}\n
               Time Step: {}\n
               '''.format(reward, reward_total, game.board, time_step))

        # action = np.argmax(q_table[state])
        board = nn.game.board
        oneindex = board.index(1)
        twoindex = board.index(2)
        state = [0] * 14
        state[oneindex] = 1
        state[twoindex + 7] = 2
        state = np.array([state])

        action = np.round(nn.think(state))
        # action = 1 - action
        reward, next_state = game.move(action)
        state = next_state
        time_step += 1
        reward_total += reward
        time.sleep(0.5)

    print('''Reward: {}\n
                       Reward Total: {}
                       State: {}\n
                       Time Step: {}\n
                       '''.format(reward, reward_total, game.board, time_step))


# DQN
import tensorflow as tf
from tensorflow import keras


class DQNAgent:
    def __init__(self):
        self.epsilon = 1.0
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.01
        self.alpha = 0.001
        self.gamma = 0.95
        self.model = self._make_model()
        self.memory = []

    def _make_model(self):
        model = keras.Sequential([
            keras.layers.Dense(units=500, activation=keras.activations.relu),
            keras.layers.Dense(units=7, activation=keras.activations.relu),
            keras.layers.Dense(units=1, activation=keras.activations.sigmoid)
        ])

        model.compile(optimizer='adam', loss="mse")
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        action = 0
        if random.random() <= self.epsilon:
            action = random.randrange(0, 2)
        else:
            action_suggestions = self.model.predict(state)
            action = np.argmax(action_suggestions)
        return action

    def replay_and_train(self, size: int):
        batch = random.sample(self.memory, size)
        for state, action, reward, next_state, done in batch:
            if reward > 0:
                target = action
            else:
                target = 1 - action
            # target_fit = self.model.predict(state)
            # print(target_fit)
            # target_fit[0][action] = target
            # print(state)
            self.model.fit(state, np.reshape(target, [1, 1]), epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon -= self.epsilon_decay


def dqn_train(episodes, max_time_step_per_episode):
    game = Game(0, 6)
    agent = DQNAgent()

    for episode in range(episodes):
        state = game.rand_board()
        state = np.reshape(state, [1, 7])
        time_steps_used = 0
        for time_step in range(max_time_step_per_episode):
            action = agent.act(state)
            reward, next_state = game.move(action)
            done = game.game_over
            next_state = np.reshape(next_state, [1, 7])
            agent.remember(state, action, reward, next_state, done)
            state = next_state
            if done:
                print("episode: {}/{}, score: {}"
                      .format(episode, episodes, time_step))
                time_steps_used = time_step
                break
        agent.replay_and_train(time_steps_used)




if __name__ == '__main__':
    # person_play()
    # ai_train()
    # ai_play(18000)
    # nn_train(100)
    # nn_play()
    dqn_train(1000, 50)

